---
layout: handbook-page-toc
title: "GitLab.com DMCA Removal Requests"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Overview

This page stipulates the workflow steps that need to be followed when we receive a Digital Millennium Copyright Act (DMCA) take down request as per the Gitlab [DMCA Policy](https://about.gitlab.com/handbook/dmca/). 

## Workflow

### First 24 hours

1. Review the notice and confirm that the content is still **live** and hosted on **Gitlab.com**
   * if the content is no longer available, proceed to `Content No Longer Available`.
   * If the content lives on a **self-hosted** GitLab instance, proceed to `Not hosted on Gitlab.com` section.
1. If the content is live and available on Gitlab.com, confirm that the notice meets the minimum requirements. 
   * If the request meets the minimum requirements, proceed to `Completing the DMCA Request Review Form` section.
1. If the request does not meet the minimum requirements, proceed to step `Need More Information` section.

### Completing the DMCA Request Review Form

1. Open up a new `DMCA_Legal_Review_Request` issue in the [DMCA REQUESTS](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/dmca/) repository.
1. Complete the form as per the instructions.
1. Notify `Legal` about the receipt of the DMCA request.
  1. Set the issue due date for 48 hours (excluding weekends. i.e., following Monday).
  1. Update the label of the issue as per the instructions.
  1. Once completed, proceed to `Notifying the Reporter::DMCA Request sent for Review`  

### Next 24 Hours

### Creating the Outbound (Zendesk)

1. Once the DMCA Request has been approved by Legal, open up a new outbound ticket in Zendesk. 
   * If legal has not reviewed the request yet, pend the issue for a further 24 hours. 
1. Create a new ticket in Zendesk 
1. Select the `DMCA::Notice to Content 'Owner'::First Touch` macro. 
1. Paste **Formatted DMCA** request into the space provided.
1. Remove `Personal Identifiable Information (PII)` of the Reporter/agent.
1. Set the requester email to the email address belonging to the GitLab.com user, the macro should set the ticket on-hold and re-open in 24 hours.
1. In the newly created Zendesk ticket, add an **internal note** referencing the `DMCA_Legal_Review_Request` Issue.
1. In the `DMCA_Legal_Review_Request` issue, post a reference link to the Zendesk Ticket. 
1. Update the issue labels as per the form instructions and set the issue due date for 48 hours (excluding weekends. i.e., following Monday)

### Next 24 Hours

1. Confirm whether the content owner has responded to the Zendesk ticket.
   * If the content owner has responded with a counter notice, proceed to `Counter Notices and user disputes`
1. If the content owner has not responded, proceed to send a follow up using the `DMCA::Notice to Content 'Owner'::Second Touch` macro.
1. Paste **Formatted DMCA** request into the space provided.
1. Remove `Personal Identifiable Information (PII)` of the Reporter/agent and forward the notice to the content owner.  
1. Update the issue labels as per the form instructions and set the issue due date for 48 hours (excluding weekends. i.e., following Monday)

### Next 24 Hours 

### Take down (No Response)

1. Confirm whether the content owner has responded to the Zendesk ticket.
   * If the content owner has responded with a complete counter notice, proceed to `Counter Notices and user disputes`
1. If the content owner has not responded, continue to disable the account as per the form instructions. 
1. Send the confirmation of take down using the `DMCA::Notice to Content 'Owner'::Final Touch (take down)` macro.
1. Paste **Formatted DMCA** request into the space provided.
1. Remove `Personal Identifiable Information (PII)` of the Reporter/agent and forward the notice to the content owner.
1. Once completed, proceed to `Notifying the Reporter::DMCA Take down Complete`

### Counter Notices and user disputes

 1. If the notice meets the minimum requirements, proceed to send the `DMCA::Acknowledge Counter Receipt` macro.
   1. If the content owner disputes the takedown request, questions can be deferred to a discussion with `Legal` in the open `DMCA_Legal_Review_Request` issue. 
   1. If the notice does not meet the minimum requirements, send the `Need More Information` blurb and pend 24 hours. 
   1. Notify `Legal` about the receipt of the Counter Notice request. 
   1. Set the issue due date for 48 hours (excluding weekends. i.e., following Monday).
   1. Update the label(s) of the issue as per the instructions on the `DMCA Legal Review Request` form.


### Notifying the Reporter 

**DMCA Request sent for Review**

  1. Notify the `Reporter` that the `DMCA` request has been sent for review using the `DMCA Request Received Blurb`
  1. Update the label of the issue as per the form instructions. 

**DMCA Review Complete** 

  1. Notify the `Reporter` that the `DMCA` has met the minimum requirements and access to the content has been removed using the `DMCA Review Complete` template.
  1. Update the `DMCA_Legal_Review_Request` issue noting that the DMCA Take Down has been completed and no further action is required.
  1. Update the labels in the issue as per the instructions on the `DMCA Legal Review Request` form. 
  1. Close the issue. 

**Counter Notice received**

1. Notify the `Reporter` that we have received a counter notice. 
1. Paste **Formatted Counter Notice** request into the space provided.
1. Remove `Personal Identifiable Information (PII)` of the content owner or representing agent and forward the notice.  
1. Update the issue labels as per the `DMCA_Legal_Review_Request` instructions and proceed to `Reactivating the content`

### Legal Review has been completed (Counter Notice)

1. Once the Counter Notice has been approved, proceed to `Notifying the Reporter:: Counter Notice Received` 

### Court order / Subpoena

1. After we have received and served a valid counter notice, the next step would be for the requestor to start litigation by submitting a court order/subpoena to legal@gitlab.com.
   * There are no further steps the Trust and Safety Team can take to assist in the takedown process. 
  
### Content no longer available

1. If the content is no longer available, respond to the requestor with the `Content no longer available` Blurb.
   * It is important to note that if the content has moved to another gitlab.com account, a new DMCA request needs to be submitted. 
   * Close the issue and update the issue with the correct labels. 

### Need More Information 

1. If the DMCA Request or Counter Notice is incomplete or missing information, request the additional information as per [DMCA Policy](https://about.gitlab.com/handbook/dmca)
   * Notices that are incomplete should not be sent for review unless all the information required by our DMCA Policy has been provided. 

### Not hosted on Gitlab.com 

1.  If the content is not hosted on Gitlab.com, respond to the requestor with the `Not hosted on Gitlab.com` blurb providing the WHOIS Information (if available) of the domain owner at the time the report was processed. 

### Reactivating the content

1. If we receive a valid Counter Notice and/or the content has been removed, restore access to the account upon the of receipt of a valid counter notice. 

### General Information

1.  Multiple DMCA Requests would not result in a secondary takedown of previously reported content. 
1.  After the receipt of a valid counter notice, access to the account needs to be restored within a maximum 14 Business days.
1.  In the event that the 24-hour period has lapsed, follow with the team member assigned to the issue.  


